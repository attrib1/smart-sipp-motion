import XCTest
@testable import SmartSippMotion4_0

class SmartSippMotion4_0Tests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(SmartSippMotion4_0().text, "Hello, World!")
    }


    static var allTests : [(String, (SmartSippMotion4_0Tests) -> () throws -> Void)] {
        return [
            ("testExample", testExample),
        ]
    }
}
