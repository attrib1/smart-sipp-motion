//
//  ViewController.swift
//  SmartSippMotion4.0
//
//  Created by PENA on 2/15/2560 BE.
//  Copyright © 2560 PENA. All rights reserved.
//

import UIKit
import FileExplorer
import FileBrowser
import CoreMotion
import CoreLocation
import GTAlertBar


class ViewController: UIViewController ,CLLocationManagerDelegate{
    
    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    var array = [String]()
    var docController:UIDocumentInteractionController!
    let manager = CMMotionManager()
    var i = 0
    var time = Timer()
    var heading = 0.0
    var Sm_len = 0
    var Sm_busStop = 0
    var vSm_len = 1
    var vSm_busStop = ""
    var head_senser = "timestamp,GyroX,GyroY,GyroZ,AcceroX,AcceroY,AcceroZ,lane,busstop"
    var head_location = "timestamp,lat,lon,speed,altitude,heading,horizontal-accuracy,vertical-accuracy,lane,busstop"
    
    
    @IBOutlet weak var tf_filename: UITextField!
    
    @IBOutlet weak var lb_acc_x: UILabel!
    @IBOutlet weak var lb_acc_y: UILabel!
    @IBOutlet weak var lb_acc_z: UILabel!
    
    @IBOutlet weak var lb_gy_x: UILabel!
    @IBOutlet weak var lb_gy_y: UILabel!
    @IBOutlet weak var lb_gy_z: UILabel!
    
    @IBOutlet weak var lb_lat: UILabel!
    @IBOutlet weak var lb_lng: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        if UserDefaults.standard.value(forKey: "tf_filename") != nil{
            tf_filename.text =  UserDefaults.standard.value(forKey: "tf_filename") as! String?
        }else{
            let str = "data"
            UserDefaults.standard.setValue(str, forKey: "info")
            tf_filename.text = str
        }
        
        
    }
    
    
    @IBAction func btn_fileSystem(_ sender: Any) {
        let fileExplorer = FileExplorerViewController()
        self.present(fileExplorer, animated: true, completion: nil)
    }
    @IBAction func btn_browse(_ sender: Any) {
        let fileBrowser = FileBrowser()
        present(fileBrowser, animated: true, completion: nil)
    }
    
    @IBAction func sm_len(_ sender: UISegmentedControl) {
        let select = sender.selectedSegmentIndex
        print(sender.selectedSegmentIndex)
        
        if select == 0 {
            vSm_len = 1
        }else if select == 1{
            vSm_len = 2
        }else if select == 2{
            vSm_len = 3
        }else if select == 3{
            vSm_len = 4
        }else{
            vSm_len = 5
        }
    }
    
    @IBAction func sm_busstop(_ sender: UISegmentedControl) {
        let select = sender.selectedSegmentIndex
        print(sender.selectedSegmentIndex)
        if select == 0{
            vSm_busStop = ""
        }else if select == 1{
            vSm_busStop =  "before"
        }else if select == 2{
            vSm_busStop = "in"
        }else if select == 3{
            vSm_busStop = "after"
        }else{
            vSm_busStop = "out"
        }
        
    }
    
    
    @IBAction func btn_start(_ sender: Any) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if (tf_filename.text?.characters.count)! >= 1 {
            UserDefaults.standard.setValue(tf_filename.text,forKey: "tf_filename")
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
            Senser()
        }else{
            AlertBar(textString: "ตัวอักษรต้องมากกว่า 1 ครับ")
        }
        
        
    }
    
    @IBAction func btn_end(_ sender: Any) {
        manager.stopGyroUpdates()
        manager.stopAccelerometerUpdates()
        locationManager.stopUpdatingLocation()
        locationManager.stopUpdatingHeading()
        time.invalidate()
    }
    
    
    
    
    
    
    
    func writeToFile(_ content: String, fileName: String,header:String) {
        
        let contentToAppend = content+"\n"
        let nHeader = "\(header)\n"
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        // let filePath = NSHomeDirectory() + "/Documents/" + fileName
        let filePath = "\(documentsPath)/\(fileName)"
        
        //Check if file exists
        if let fileHandle = FileHandle(forWritingAtPath: filePath) {
            //Append to file
            fileHandle.seekToEndOfFile()
            fileHandle.write(contentToAppend.data(using: String.Encoding.utf8)!)
            
            
        }else {
            //Create new file
            do {
                try nHeader.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
                //                if let fileHandle = FileHandle(forWritingAtPath: filePath) {
                //                    //Append to file
                //
                //                    fileHandle.seekToEndOfFile()
                //                    fileHandle.write(nHeader.data(using: String.Encoding.utf8)!)
                //
                //
                //                }else{
                //                    print("Error write header \(filePath)")
                //                }
            } catch {
                print("Error creating \(filePath)")
            }
        }
        // print("save file")
    }//end weitefile
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        heading = newHeading.magneticHeading
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0]
        let lat = String(userLocation.coordinate.latitude)
        let lng = String(userLocation.coordinate.longitude)
        let time = String(describing: userLocation.timestamp)
        let altitude   = String(userLocation.altitude)
        let speed   = String(userLocation.speed)
        let verticalAccuracy = String(userLocation.verticalAccuracy)
        let horizontalAccuracy = String(userLocation.horizontalAccuracy)
        
        
        lb_lat.text = String(format: "%.6f", userLocation.coordinate.latitude)
        lb_lng.text = String(format: "%.6f", userLocation.coordinate.longitude)
        
        let location = "\(time),\(lat),\(lng),\(speed),\(altitude),\(heading),\(verticalAccuracy),\(horizontalAccuracy),\(vSm_len),\(vSm_busStop)"
        
        
        writeToFile(location, fileName: "\(UserDefaults.standard.value(forKey: "tf_filename")!)-location.csv",header: self.head_location)
        
    }//end location
    
    func Senser(){
        if manager.isDeviceMotionAvailable{
            self.manager.startGyroUpdates()
            self.manager.startAccelerometerUpdates()
            time = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) {
                (_) in
                
                
                if self.manager.gyroData?.rotationRate != nil
                    && self.manager.accelerometerData?.acceleration != nil{
                    self.i += 1
                    //var data = "\(self.i),"
                    
                    var data = "\(Date()),"
                    if let gyr = self.manager.gyroData?.rotationRate{
                        self.lb_gy_x.text = String(format: "%.7f", gyr.x)
                        self.lb_gy_y.text = String(format: "%.7f", gyr.y)
                        self.lb_gy_z.text = String(format: "%.7f", gyr.z)
                        
                        data += "\(gyr.x),\(gyr.y),\(gyr.z)"
                    }
                    if let acc = self.manager.accelerometerData?.acceleration{
                        self.lb_acc_x.text = String(format: "%.7f", acc.x)
                        self.lb_acc_y.text = String(format: "%.7f", acc.y)
                        self.lb_acc_z.text = String(format: "%.7f", acc.z)
                        data += ",\(acc.x),\(acc.y),\(acc.z)"
                        
                    }
                    
                    
                    data += ",\(self.vSm_len),\(self.vSm_busStop)"
                    
                    self.writeToFile(data, fileName: "\(UserDefaults.standard.value(forKey: "tf_filename")!)-sensor.csv",header: self.head_senser)
                }
                
                
                
            }
            
            
        }//end manager
    }
    
    
    
    func AlertBar(textString:String) {
        let options = GTAlertBarOptions()
        options.colors.background = UIColor.red
        options.image = GTAlertBarImage.exclamation
        GTAlertBar.barAttachedToView(self,title: "ผิดพลาด !!!",body: textString,options: options)
        
    }
    
    
    
}//end class

